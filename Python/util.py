from functools import wraps


class Config:
    pass


class Timeout(Exception):
    pass


def print_on_call(func, cls):
    @wraps(func)
    def wrapper(*args, **kw):
        print('{}.{} called'.format(cls.__name__, func.__name__))
        try:
            res = func(*args, **kw)
        finally:
            print('{}.{} finished'.format(cls.__name__, func.__name__))
        return res
    return wrapper


def fault_prone(function_decorator):
    def decorator(cls):
        for name, obj in vars(cls).items():
            if callable(obj):
                try:
                    obj = obj.__func__  # unwrap Python 2 unbound method
                except AttributeError:
                    pass  # not needed in Python 3
                setattr(cls, name, function_decorator(obj, cls))
        return cls
    return decorator
