from dataclasses import dataclass
from util import *


@fault_prone(print_on_call)
@dataclass
class Component:
    isUp = True
    errorBacklog = []


@fault_prone(print_on_call)
@dataclass
class Notification:
    def notify(self, msg: str):
        print("USER_NOTIFICATION: " + msg)


@fault_prone(print_on_call)
@dataclass
class Supply:
    bought = False

    def buy(self):
        if not self.bought:
            # some action
            self.bought = True

    def compensate(self):
        if self.bought:
            # some action
            self.bought = False


@fault_prone(print_on_call)
@dataclass
class Warehouse:
    itemsMoved = False

    def moveItems(self):
        if not self.itemsMoved:
            # some action
            self.itemsMoved = True

    def compensate(self):
        if self.itemsMoved:
            # some action
            self.itemsMoved = False


@fault_prone(print_on_call)
@dataclass
class Inventory:
    stockDecreased = False
    lowStock = True
    supply: Supply
    warehouse: Warehouse

    def decreaseStock(self):
        if not self.stockDecreased:
            # some actions
            self.stockDecreased = True

        try:
            if self.lowStock:
                self.supply.buy()

            self.warehouse.moveItems()
        except Timeout:
            self.supply.compensate()
            self.warehouse.compensate()

        return True

    def compensate(self):
        if self.stockDecreased:
            # some action
            self.warehouse.compensate()
            self.supply.compensate()
            self.stockDecreased = False


@fault_prone(print_on_call)
@dataclass
class Wallet:
    reducedFromBalance = False

    def reduceBalance(self):
        if not self.reducedFromBalance:
            # some actions
            self.reducedFromBalance = True
        return True

    def compensate(self):
        if self.reduceBalance:
            # some actions
            self.reduceBalance = False


@fault_prone(print_on_call)
@dataclass
class Order(Component):
    wallet: Wallet
    inventory: Inventory
    notification: Notification

    def processOrder(self):
        try:
            self._callWallet()
            self._callInventory()
        except Timeout:
            self.wallet.compensate()
            self.inventory.compensate()

        self.notification.notify("Your order is processed.")

    def _callWallet(self):
        if self.wallet.reduceBalance() == False:
            self.notification.notify("Low balance")
            self.inventory.compensate()

    def _callInventory(self):
        if self.inventory.decreaseStock() == False:
            self.notification.notify("Low stock")
            self.wallet.compensate()
