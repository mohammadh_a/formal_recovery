from component import *


class App:
    def execute(self):
        notification = Notification()
        supply = Supply()
        warehouse = Warehouse()
        wallet = Wallet()
        inventory = Inventory(supply=supply, warehouse=warehouse)
        order = Order(wallet=wallet, inventory=inventory,
                      notification=notification)

        order.processOrder()


if __name__ == '__main__':
    App().execute()
