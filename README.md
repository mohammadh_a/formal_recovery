# Formal Recovery

Repository for thesis about using formal verification to check soundness of failure recovery methods.

## Getting started

Download `.rebeca` and `.property` files and run it in afra model checker.