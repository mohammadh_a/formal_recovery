package network;

import component.CreditManager;
import component.Dispatcher;
import component.Handler;
import component.Output;
import system.Order;
import system.Trade;

import java.util.List;

public interface Network {
    boolean sendOrderToHandler(Order order, Handler handler);
    boolean askCredit(Order buyOrder , CreditManager creditManager);
    boolean changeCredit(int buyerBrokerId,int sellerBrokerId, int value, CreditManager creditManager);
    boolean sendTradeToOutput(Trade trade, Output output);
    boolean sendMessageToOutput(String message, Output output);
    List<Trade> askSentTradesByHandlerToOutput(Handler handler,Output output);
}
