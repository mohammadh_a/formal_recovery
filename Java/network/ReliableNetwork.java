package network;

import component.CreditManager;
import component.Dispatcher;
import component.Handler;
import component.Output;
import system.Order;
import system.Trade;

import java.util.List;

public class ReliableNetwork implements Network {
    @Override
    public boolean askCredit(Order buyOrder, CreditManager creditManager) {
        return creditManager.hasCredit(buyOrder);
    }

    @Override
    public boolean changeCredit(int buyerBrokerId, int sellerBrokerId, int value, CreditManager creditManager) {
        return creditManager.commitTransaction(buyerBrokerId,sellerBrokerId,value);
    }

    @Override
    public boolean sendTradeToOutput(Trade trade, Output output) {
        output.addTrade(trade);
        return true;
    }

    @Override
    public boolean sendMessageToOutput(String message, Output output) {
        output.addMessage(message);
        return false;
    }

    @Override
    public boolean sendOrderToHandler(Order order, Handler handler) {
        handler.addOrder(order);
        return true;
    }

    @Override
    public List<Trade> askSentTradesByHandlerToOutput(Handler handler, Output output) {
        return output.getTradesSentByHandler(handler);
    }
}
