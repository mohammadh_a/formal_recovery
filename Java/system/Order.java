package system;

import java.util.List;

public class Order implements Comparable<Order>{
    static int idCounter=0;
    public enum OrderSide {
        BUY,SELL
    }
    private int orderId;
    private OrderSide side;
    private int securityId;
    private int brokerId;
    private int value;

    public Order(OrderSide side, int securityId, int brokerId, int value) {
        this.orderId = ++idCounter;
        this.side = side;
        this.securityId = securityId;
        this.brokerId = brokerId;
        this.value = value;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public OrderSide getSide() {
        return side;
    }

    public void setSide(OrderSide side) {
        this.side = side;
    }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }

    public int getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(int brokerId) {
        this.brokerId = brokerId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(Order o) {
        return getValue() - o.getValue();
    }
}
