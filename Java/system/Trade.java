package system;

public class Trade {
    static int tradeCounter =0;
    int tradeId;
    Order order;
    int buyerBrokerId;

    public Trade(Order order, int buyerBrokerId){
        this.order = order;
        this.tradeId= ++tradeCounter;
        this.buyerBrokerId = buyerBrokerId;
    }

    public Trade(int tradeId, Order order, int buyerBrokerId) {
        tradeCounter++;
        this.tradeId = tradeId;
        this.order = order;
        this.buyerBrokerId = buyerBrokerId;
    }

    public int getTradeId() {
        return tradeId;
    }

    public void setTradeId(int tradeId) {
        this.tradeId = tradeId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "BuyerBrokerId: " + buyerBrokerId + "\t SellerBrokerId: "+ order.getBrokerId()
                +"\nSecurityId: " + getOrder().getSecurityId() + "\t Value: "+order.getValue();
    }
}
