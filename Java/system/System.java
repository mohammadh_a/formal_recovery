package system;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import component.*;
import network.Network;
import network.ReliableNetwork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class System {
    public static final String INPUT_FILE = "./src/input-01.json";

    public static void main(String[] args) {
        Network network = new ReliableNetwork();
        CreditManager creditManager = new CreditManager();
        creditManager.addBrokerCredit(1,100);
        creditManager.addBrokerCredit(2,100);
        creditManager.addBrokerCredit(3,100);

        Output output = new Output();

        Handler handler1 = new Handler(100, network, creditManager, output, new JournalFile());
        Handler handler2 = new Handler(101, network, creditManager, output, new JournalFile());

        Dispatcher dispatcher = new Dispatcher(Arrays.asList(handler1, handler2), new JournalFile());
        dispatcher.dispatch(parseOrdersFromInput());
    }

    private static List<Order> parseOrdersFromInput() {
        List<Order> result = new ArrayList<>();
        Reader reader = null;
        try {
            reader = new BufferedReader(new FileReader(INPUT_FILE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        JsonObject inputJsonObject = JsonParser.parseReader(reader).getAsJsonObject();
        for (JsonElement orderJson : inputJsonObject.get("orders").getAsJsonArray()) {
            String side = orderJson.getAsJsonObject().get("side").getAsString();
            int brokerId = orderJson.getAsJsonObject().get("brokerId").getAsInt();
            int securityId = orderJson.getAsJsonObject().get("securityId").getAsInt();
            int value = orderJson.getAsJsonObject().get("value").getAsInt();
            result.add(new Order(side.equals("SELL") ? Order.OrderSide.SELL : Order.OrderSide.BUY,
                    securityId, brokerId, value));
        }
        return result;
    }
}
