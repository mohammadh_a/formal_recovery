package component;

import system.Order;

import java.util.List;

public class JournalFile {
    List<Order> orders;

    public void saveOrder(Order order){
        orders.add(order);
    }

    public List<Order> getOrders() {
        return orders;
    }

}
