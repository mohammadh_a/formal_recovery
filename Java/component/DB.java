package component;

import system.Order;

import java.util.ArrayList;
import java.util.List;

public class DB {
    private static DB db = null;
    private DB() {}

    public static DB getInstance() {
        if (db == null) {
            db = new DB();
        }
        return db;
    }
    public List<Order> getStartingSellOrderQueue(){
        return new ArrayList<>();
    }
}
