package component;

import system.Order;

import java.util.HashMap;
import java.util.Map;

public class CreditManager {
    Map<Integer, Integer> creditMap;        // brokerId -> Credit
    Map<Integer, Boolean> requestCache;     // orderId -> previousResult
    public CreditManager() {
        creditMap = new HashMap<>();
        requestCache= new HashMap<>();
    }

    public void addBrokerCredit(int brokerId, int credit) {
        creditMap.put(brokerId, credit);
    }

    public boolean hasCredit(Order buyOrder) {
        if (requestCache.containsKey(buyOrder.getOrderId())){
            return requestCache.get(buyOrder.getOrderId());
        }
        int brokerId = buyOrder.getBrokerId();
        int credit = buyOrder.getValue();
        boolean result = creditMap.get(brokerId) >= credit;
        requestCache.put(buyOrder.getOrderId(),result);
        return result;
    }

    public boolean commitTransaction(int buyerBrokerId, int sellerBrokerId, int value) {
        int buyerCredit = creditMap.get(buyerBrokerId);
        int sellerCredit = creditMap.get(sellerBrokerId);
        creditMap.put(buyerBrokerId, buyerCredit - value);
        creditMap.put(sellerBrokerId, sellerCredit + value);
        return true;
    }

}
