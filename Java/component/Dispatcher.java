package component;

import network.Network;
import system.Order;

import java.util.List;

public class Dispatcher {
    List<Handler> handlers;
    JournalFile journalFile;
    Network network;

    public Dispatcher(List<Handler> handlers, JournalFile journalFile) {
        this.handlers = handlers;
        this.journalFile = journalFile;
    }
    public void dispatch(List<Order> orders){
        for (Order order : orders) {
            for (Handler handler : handlers) {
                //dispatch orders to handlers based on securityId
                if(order.getSecurityId()== handler.getSecurityId()){
                    network.sendOrderToHandler(order,handler);
                }
            }
        }
    }

}
