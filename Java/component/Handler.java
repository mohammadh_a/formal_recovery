package component;

import exception.LowCreditException;
import exception.ProcessingException;
import network.Network;
import system.Order;
import system.Trade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Handler {
    //    List<Order> buyOrders;
    // we dont have buy queue, only sell queue.
    // buy checks if it can match (has credit). either make trade or reject.
    List<Order> sellOrdersQueue = new ArrayList<>();
    CreditManager creditManager;
    Network network;
    Output output;
    int securityId;
    JournalFile journalFile;
    DB db;
    // ---- recovery ----
    boolean reconstructionMode = false;
    List<Trade> alreadySentToOutput = new ArrayList<>();

    public Handler(int securityId, Network network, CreditManager creditManager, Output output, JournalFile journalFile) {
        this.securityId = securityId;
        this.network = network;
        this.creditManager = creditManager;
        this.output = output;
        this.journalFile = journalFile;
        db = DB.getInstance();
    }

    public void addOrder(Order order) {
        if (order.getSide() == Order.OrderSide.BUY) {
            try {
                //try to match trade and forward it.
                Trade trade = matchOrder(order);    // FAIL : processing error

                if (!reconstructionMode) {
                    //double check where to write to journal
                    journalFile.saveOrder(order);       // TODO: Failure here: saving to journal
                    //Checkpoint journaling
                }
                if (!(reconstructionMode && isTradeAlreadySentToOutput(trade))) {
                    network.sendTradeToOutput(trade, output); //TODO: failure here: network error
                }
            } catch (LowCreditException e) {
                //reject
                network.sendMessageToOutput("Order with ID " + order.getOrderId() +
                        " is rejected. Cause: Insufficient balance.", output);
            } catch (ProcessingException e) {
                reconstructOrderbook(); //future: failure here
            }
        } else if (order.getSide() == Order.OrderSide.SELL) {
            // add to sell queue
            // future: maybe there is already a buy queue with low price that this sell order can match
            sellOrdersQueue.add(order);
            journalFile.saveOrder(order);       // TODO: Failure here: saving to journal
            Collections.sort(sellOrdersQueue);
        }
    }

    private void reconstructOrderbook() {
        reconstructionMode = true;
        //reconstruct orderbook
        sellOrdersQueue = db.getStartingSellOrderQueue();
        List<Order> journalOrders = journalFile.getOrders();
        alreadySentToOutput = network.askSentTradesByHandlerToOutput(this, output);
        for (Order journalOrder : journalOrders) {
            addOrder(journalOrder);
        }
        reconstructionMode = false;
    }

    private boolean isTradeAlreadySentToOutput(Trade trade) {
        for (Trade t : alreadySentToOutput) {
            if (t.getTradeId() == trade.getTradeId()) {
                return true;
            }
        }
        return false;
    }

    private Trade matchOrder(Order buyOrder) throws LowCreditException, ProcessingException {
        // its buy, ask CM if broker has the value,if yes, decrease its credit , make trade and increase credit of other broker
        // if no, reject this order

        boolean hasCredit = network.askCredit(buyOrder, creditManager);
        if (!hasCredit) {
            throw new LowCreditException();
        }
        for (int i = 0; i < sellOrdersQueue.size(); i++) {
            Order sellOrder = sellOrdersQueue.get(i);
            if (buyOrder.getValue() == sellOrder.getValue()) {
                sellOrdersQueue.remove(i);
                network.changeCredit(buyOrder.getBrokerId(), sellOrder.getBrokerId(), buyOrder.getValue(), creditManager);
                return new Trade(sellOrder, buyOrder.getBrokerId());
            }
        }
        return null;
    }


    public int getSecurityId() {
        return securityId;
    }

}
