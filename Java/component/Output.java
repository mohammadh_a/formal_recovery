package component;

import system.Trade;

import java.util.ArrayList;
import java.util.List;

public class Output {
    List<Trade> trades= new ArrayList<>();

    public void addTrade(Trade trade){
        trades.add(trade);
        System.out.println("Trade came to output:\n"+trade);
    }

    public void addMessage(String message){
        System.out.println("Message came to output:\n"+message);
    }

    public List<Trade> getTradesSentByHandler(Handler handler){
        List<Trade> result = new ArrayList<>();
        for (Trade trade : trades) {
            if(trade.getOrder().getSecurityId() == handler.getSecurityId()){
                result.add(trade);
            }
        }
        return trades;
    }
}
