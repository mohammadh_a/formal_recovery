package exception;

public class LowCreditException extends Exception {
    public LowCreditException(String message) {
        super(message);
    }

    public LowCreditException() {
        super();
    }
}
